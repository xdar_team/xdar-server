let router = require('express').Router();

// Import meeting controller
var meetingController = require('./controllers/meeting');
var modelController = require('./controllers/model');

// Meeting routes
router.route('/meeting')
    .get(meetingController.index)
    .post(meetingController.new);

router.route('/meeting/:meeting_id')
    //.get(meetingController.view)
    .patch(meetingController.update)
    .put(meetingController.update)
    .delete(meetingController.delete);


router.route('/meeting_1/:meeting_id')
    .get(meetingController.view)
// Model routes
router.route('/model')
.get(modelController.index)
//.post(modelController.new);

router.route('/model/:model_id')
.get(modelController.view)
.patch(modelController.update)
.put(modelController.update)
.delete(modelController.delete);


// Export API routes
module.exports = router;