const path = require('path')

const MEETING_ID = "5bebef457c400accf534af8d"
const DEFAULT_SCALE = "1.0"

const S3_BUCKET = "pw-ar"
const AWS_ACCESS_KEY_ID = "AKIAIJJ42SVXJWNHQLWQ"
const AWS_SECRET_ACCESS_KEY = "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"
const AWS_DEFAULT_REGION = 'eu-central-1'

const UPLOAD_DIR = path.join(__dirname, '..', 'uploads')

const UPDATE = {
    MODEL:                  0,
    LIGHT:                  1,
    SCALE:                  2,
    DELETE_MODEL:           3,
    MODEL_POSE:             4,

    ADD_IMAGE:              5,
    CHANGE_IMAGE:           6,
    DELETE_IMAGE:           7,
    IMAGE_ANNOTATION:       8,

    REGISTER_DEVICE:        9,
    DELETE_DEVICE:          10,

    ADD_VIEW:               11,
    
    POINT:                  12,

    SHARE:                  13,
    STOP_SHARE:             14,
    LOCK:                   15,
    STOP_LOCK:              16,

    QUICK_ANNOTATION_START: 17,
    QUICK_ANNOTATION:       18,
    QUICK_ANNOTATION_STOP:  19,

    SETTINGS:               20,
    LOCKED_VIEW:            21,
    FORCE_STOP:             22,

    LIGHT_LOG:              23,
    LOAD_VIEW:              24,
    SWITCH_MODE:            25,
}

UPDATE_NAMES = Object.keys(UPDATE)

module.exports = {
    MEETING_ID,
    DEFAULT_SCALE,

    S3_BUCKET,
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY,
    AWS_DEFAULT_REGION,
    UPLOAD_DIR,

    UPDATE,
    UPDATE_NAMES,
}