"use strict"
const mongoose = require('mongoose')
const uuidv1 = require('uuid/v1')
const Meeting = require('./models/meeting')
const Model = require('./models/model')
const Logs = require('./models/logs')
const aws = require('aws-sdk')
const Const = require('./constants')
const WebSocket = require('ws');


//aws
const s3 = new aws.S3({
	accessKeyId : Const.AWS_ACCESS_KEY_ID,
	secretAccessKey : Const.AWS_SECRET_ACCESS_KEY,
	region : Const.AWS_DEFAULT_REGION
})

var current_image_index = 0
var wss;
class RoutesHanlder {

    constructor(wss){

        mongoose.connect('mongodb://arkiteam:arkiteam1@ds159185.mlab.com:59185/xdar-db', {useNewUrlParser: true})
        this.db = mongoose.connection;
        // this.ws_clients = ws_clients;
        this.wss = wss
    }

    log(deviceId, deviceName, type, msg){

        let logEntry = new Logs();
        logEntry.deviceId = deviceId
        logEntry.deviceName = deviceName
        logEntry.type = type
        logEntry.msg = msg
        
        // save log entry
        logEntry.save((err) => { if (err) return console.log("ERORR SAVING LOG", err) })

        console.log("LOGGED" +  type);
    }

    index(req, res) {

        this.db.collection('meetings').find().toArray((err, meetings) => {

            let meetings_list = {}
            meetings.forEach(meeting => { meetings_list[meeting._id] = meeting.name })
            res.render('meetings_list.hbs', { meetings_list : meetings_list })
        })
    }

    async details_page(req, res){
       

        let meeting = await Meeting.findById(Const.MEETING_ID);
        let models_ids = meeting.models
        let models_scales = meeting.scales

        let models = await Model.find({'uuid': { $in: models_ids }})
        let models_names = models_ids.map((model_id) => {

            let model = models.find(e => e.uuid === model_id)
            if(!model) return ''
            else return model.name
        })


        //compose models details for webview
        let models_details = {}
        for(var i = 0; i < models_names.length; i++){
            models_details[i] = {
                "name" : models_names[i],
                "scale" : models_scales[i],
            }
        }

        //return meeting detail page
        return res.render('meeting_details.hbs', {
            meeting_id : Const.MEETING_ID,
            models : models_details
        })
    }

    update_light1(req, res){
        
        let light_value = req.body.light_val
        
        console.log("\nLIGHT CHANGE REQUEST: ", light_value)
        
        //inform clients of light change via ws
        let updateMsg = JSON.stringify({
            type : Const.UPDATE.LIGHT,
            value : light_value
        })

        this.sendWSUpdate(updateMsg)
        
        res.sendStatus(200)
    }

    async screenshots_page(req, res){

        let meeting = await Meeting.findById(Const.MEETING_ID);

        let screenshots_urls =  meeting.screenshots
        
        //for(let i =0; i<10; i++)
            //screenshots_urls.push(screenshots_urls[i])
        
        //return meeting detail page
        return res.render('screenshots.hbs', {
            first_image : screenshots_urls[0],
            screenshots_urls : screenshots_urls
        })
    }


    upload_model(req, res){

        // get parameters
        let meeting_id = req.body.meeting_id
        let target_id = req.body.target_id
        let model_file = req.files.file
        let model_name = model_file.name

        //generate unique name for file
        let file_uid = uuidv1()
        let file_ext = '.' + model_name.split('.').pop()
        let file_name = file_uid + file_ext
        let file_dst_path = Const.UPLOAD_DIR + '/' + file_name

        //create new model and save details
        let model = new Model();
        model.name = model_name
        model.uuid = file_uid
        model.path = file_dst_path

        // save the model and check for errors
        model.save((err) => { if (err) return res.json({error: err}) })

        //update meeting models list
        Meeting.findById(Const.MEETING_ID, (err, meeting) => {

            let models = meeting.models
            let scales = meeting.scales

            models[target_id] = file_uid
            scales[target_id] = Const.DEFAULT_SCALE

            meeting.models = models
            meeting.scales = scales

            Meeting.updateOne({ _id: meeting_id }, meeting, (err) => {

                if(err) return res.json({
                    message: "something went wrong",
                    error: err
                })

                model_name = model_name.substring(0, model.name.length -4)

                //inform client of the change via WS
                let updateMsg = JSON.stringify({
                    type : Const.UPDATE.MODEL, 
                    modelName : model_name, 
                    targetId: target_id
                })
                this.sendWSUpdate(updateMsg)

                this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], updateMsg)

                //success
                return res.sendStatus(200)     
            })
        })
    }

    async upload_screenshot(req, res){

        console.log("UPLOADING SCREENSHOT..")
        //fs.writeFile(path.join(UPLOAD_DIR, req.files.screenshot_file.name), req.files.screenshot_file.data, (err)  => { if (err) throw err })

        let img_uid = uuidv1()
        const s3Params = {
            Bucket: Const.S3_BUCKET,
            Key: img_uid,
            Body : req.files.screenshot_file.data,
            Expires: 60,
            ContentType: req.files.screenshot_file.mimetype,
            ACL: 'public-read',
            region: 'eu-central-1'
        }

        s3.upload(s3Params, async (err, data) => {
            
            if(err) console.log("S3 - UPLOAD FAILED: ", err)

            console.log("S3 - IMAGE URL: ", data.Location)

            //Update screenshots list
            let meeting = await Meeting.findById(Const.MEETING_ID);
            meeting.screenshots.push(data.Location)
            await Meeting.updateOne({ _id: Const.MEETING_ID }, meeting)

            //notify client
            let updateImageMsg = JSON.stringify({
                type: Const.UPDATE.ADD_IMAGE,
                url: data.Location
            })

            this.sendWSUpdate(updateImageMsg)

            this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], updateImageMsg)
            
            console.log("UPLOAD COMPLETE")
            
            return res.json({
                status: "OK - UPLOAD COMPLETE",
                details: data
            })
        })
    }
    
    async save_view(req, res){

        console.log("SENDING VIEW TO CLIENTS")
        var view = req.body.view;

        //Update views list
        let meeting = await Meeting.findById(Const.MEETING_ID);
        meeting.views.push(view)
        await Meeting.updateOne({ _id: Const.MEETING_ID }, meeting)

        //notify client
        let addView = JSON.stringify({ view: view })

        this.sendWSUpdate(view)
        
        this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], view)
        
        return res.json({
            status: "OK - VIEW SENT",
            details: view
        })
    }

    send_point_update(pointUpdate){

        console.log("POINT UPDATE")
        this.sendWSUpdate(JSON.stringify(pointUpdate))
    }

    send_share_update(shareUpdate){

        console.log("SHARE UPDATE")
        this.sendWSUpdate(JSON.stringify(shareUpdate))
    }

    send_stop_share_update(stopShareUpdate){

        console.log("STOP UPDATE")
        this.sendWSUpdate(JSON.stringify(stopShareUpdate))
    }

    send_lock_update(lockUpdate){

        console.log("LOCK UPDATE")
        this.sendWSUpdate(JSON.stringify(lockUpdate))
    }

    send_stop_lock_update(stopLockUpdate){

        console.log("STOP LOCK UPDATE")
        this.sendWSUpdate(JSON.stringify(stopLockUpdate))
    }

    send_quick_annotation_start(annotationStartUpdate){

        console.log("QUICK ANNOTATION START")
        this.sendWSUpdate(JSON.stringify(annotationStartUpdate))
    }
    
    send_quick_annotation_point(annotationPointUpdate){

        console.log("QUICK ANNOTATION POINT")
        this.sendWSUpdate(JSON.stringify(annotationPointUpdate))
    }

    send_quick_annotation_stop(annotationStopUpdate){

        console.log("QUICK ANNOTATION STOP")
        this.sendWSUpdate(JSON.stringify(annotationStopUpdate))
    }

    send_new_settings(req, res){

        console.log("NEW SETTINGS")

        var newSettings = req.body.settingsUpdate
        this.sendWSUpdate(newSettings)

        this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], newSettings)       

        return res.sendStatus(200)     
    }

    send_locked_view_update(req, res){

        console.log("LOCKED VIEW")

        var lockedViewUpdate = req.body.viewUpdate;
        this.sendWSUpdate(lockedViewUpdate)

        this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], lockedViewUpdate)       

        return res.sendStatus(200)     
    }

    send_force_stop(req, res){

        console.log("FORCE STOP STREAM VIEW")

        var forceStopUpdate = req.body.forceStopUpdate;
        this.sendWSUpdate(forceStopUpdate)

        this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], forceStopUpdate)

        return res.sendStatus(200)     
    }

    update_scale(req, res){
        
        let new_scale = req.body.scale
        let target_id = req.body.target_id
        
        console.log("UPDATING SCALE: target_id", target_id, " TO ", new_scale)
        
        //find target meeting and updates models scales
        Meeting.findById(Const.MEETING_ID, (err, meeting) => {
            
            meeting.scales[target_id] = new_scale
            
            Meeting.updateOne({ _id: Const.MEETING_ID }, meeting, (err) => {
                if(err) return res.json({message: "something went wrong", error: err })
                
                //inform clients of scale change via ws
                let updateMsg = JSON.stringify({
                    type : Const.UPDATE.SCALE, 
                    targetId: target_id,
                    value : new_scale
                })
                
                this.sendWSUpdate(updateMsg)
                
                this.log(req.body.deviceId,req.body.deviceName, Const.UPDATE_NAMES[req.body.type], updateMsg)       
                
                return res.sendStatus(200)
            })
        })
    }
    

    update_light(lightUpdate){

        console.log("LIGHT UPDATE")
        this.sendWSUpdate(JSON.stringify(lightUpdate))
    }
    
    update_main_image(wsUpdate){

        console.log("CHANGE IMAGE UPDATE")
        this.sendWSUpdate(JSON.stringify(wsUpdate))
    }
    
    async meeting_details(req, res){

        let m_id  = req.params.meeting_id 
        let meeting = await Meeting.findById(m_id);
        let models_ids = meeting.models
        let models_scales = meeting.scales

        let models = await Model.find({'uuid': { $in: models_ids }});

        let models_names = models_ids.map((model_id) => {
            let model = models.find(e => e.uuid === model_id);
            if( ! model) return '';
            else return model.name.substring(0, model.name.length -4)

        })

        let models_uids = models_ids.map((model_id) => {
            let model = models.find(e => e.uuid === model_id);
            if(!model) return '0';
            else return model.uuid; 
        })

        return res.json( {
            names: models_names,
            uuids: models_uids,
            scales : models_scales
        })
    }

    async meeting_screenshots_list(req, res){

        let meeting_id = req.params.meeting_id

        try{

            let meeting = await Meeting.findById(meeting_id);
            let screenshots_urls = meeting.screenshots
    
            console.log(meeting_id, " SCREENSHOTS: ", screenshots_urls.length)
    
            return res.json({
                urls: screenshots_urls,
                current_image : current_image_index
            })
        }catch(err) {return res.send("INVALID MEETING ID:" + meeting_id)}
    }


    resetMeeting(req, res){

        current_image_index = 0;
        return res.json({"status": "OK", "CURRENT_IMAGE_INDEX": current_image_index})
    }

    /* UTILS */
    
    //send message to all ws clients
    sendWSUpdate(msg){
        
        console.log("WS - SENDING MESSAGE:\n", msg)
        
        // for(var device_id in this.ws_clients){

        //     if(this.ws_clients[device_id].readyState === WebSocket.OPEN){

        //         console.log("\tWS - SENDING TO ", device_id )
        //         this.ws_clients[device_id].send(msg)
        //     }
        // }

        var i = 0;
        
        this.wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {

                console.log("\tWS - SENDING TO " + i)
                client.send(msg);
                 
                i++
            }
        })

        console.log("WS -  DONE\n\n")
    }
}

module.exports = RoutesHanlder