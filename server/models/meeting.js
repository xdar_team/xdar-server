
var mongoose = require('mongoose')

// Setup schema
var meetingSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    models: {
        type: Array,
        required: false
    },
    scales: {
        type: Array,
        required: false
    },
    screenshots : {
        type: Array,
        required: false
    },
    views : {
        type: Array,
        required: false
    }
});

// Export Meeting model
var Meeting = module.exports = mongoose.model('meeting', meetingSchema)

module.exports.get = function (callback, limit) {
    Meeting.find(callback).limit(limit)
}
