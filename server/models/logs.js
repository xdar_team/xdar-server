
var mongoose = require('mongoose')

// Setup schema
var logsSchema = mongoose.Schema({
        deviceId: {
            type: String,
            required: false
        },
        deviceName: {
            type: String,
            required: false
        },
        type: {
            type: String,
            required: false
        },
        msg : {
            type: String,
            required: false
        }
    },
    {timestamps: true}
);

// Export Logs model
var Logs = module.exports = mongoose.model('logs', logsSchema)

module.exports.get = function (callback, limit) {
    Logs.find(callback).limit(limit)
}
