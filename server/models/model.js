
var mongoose = require('mongoose');

// Setup schema
var modelSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    uuid : {
        type: String,
        required: true
    }
});

// Export model
var Model = module.exports = mongoose.model('model', modelSchema)

module.exports.get = function (callback, limit) {
    Model.find(callback).limit(limit)
}
