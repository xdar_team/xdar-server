// Import model model
let path = require("path")
const uuidv1 = require('uuid/v1')
let Model = require('../models/model')

let upload_dir = path.join(__dirname, '..', '..', 'uploads')

// INDEX: return all models
exports.index = function (req, res) {
    Model.get(function (err, models) {
        if (err) 
            return res.json({
                message: "Somethings is broken",
                error: err,
            });
        
        return res.json({
            message: "Models retrieved successfully",
            data: models
        });
    });
};


/*
// POST: upload model file and save its details in model collection
exports.new = function (req, res) {

    console.log("HEREEEEE")
    if (Object.keys(req.files).length == 0) return res.status(400).send('No files were uploaded.')
    
    console.log("REQUEST", req.body, req.files)
    // retrive the uploaded file
    let model_file = req.files.file;
    let model_name = model_file.name
    
    let file_ext = '.' + model_name.split('.').pop()

    //generate unique id as new name for file
    let file_uid = uuidv1()

    let file_name = file_uid + file_ext
    let file_dst_path = upload_dir + '/' + file_name


    // move the model file to the uploads folder
    model_file.mv(file_dst_path, function(err) {
        if (err){
            
            return res.json({
            message : "something went wrong, ops",
            uploading_to : file_dst_path,
            error: err,
            })
        }
    })

    //register new file info in models collection

    var model = new Model();
    model.name = file_name
    model.uuid = file_uid
    model.path = file_dst_path

    // save the model and check for errors
    model.save(function (err) {
        if (err)
            return res.json({
                message: "Something went wrong while creating new model",
                error: err
            });

        //update meeting info
        

        
        return res.json({
            message: 'New model created!',
            data: model
        });
    });
};*/


// GET: retrieve model info by model_id
exports.view = function (req, res) {
    Model.findById(req.params.model_id, function (err, model) {
        if (err || model == null)
            return res.json({
                message: 'Model not found',
                error: err
            })

        return res.json({
            message: 'Model details',
            data: model
        });
    });
};

// UPDATE: update model info by model_id
exports.update = function (req, res) {
    Model.findById(req.params.model_id, function (err, model) {
        if (err || model == null)
            return res.json({
                message: "Model not found",
                error: err
            });


        model.name = req.body.name;
        model.path = req.body.path;
        
        // save the model and check for errors
        model.save(function (err) {
            if (err)
                return res.json({
                    message: "Something went wrong while updating model",
                    error: err
                });
            return res.json({
                message: 'Model Info updated',
                data: model
            });
        });
    });
};

// DELETE: delete model by model_id
exports.delete = function (req, res) {
    Model.deleteOne({_id: req.params.model_id}, function (err, model) {
        if (err)
            return res.json({
                message: "Something went wrong",
                error: err
            });

        return res.json({message: 'Model deleted'});
    });
};