// Import meeting model
Meeting = require('../models/meeting')
Model = require('../models/model')

let default_target = "0"
let default_scale = "1.0"

// INDEX: return all meetings - name and id for each meeting
exports.index = function (req, res) {

    console.log("MEETING DETAILS REQUEST")

    Meeting.get(function (err, meetings) {
        if (err) 
            return res.json({
                message: "Somethings is broken",
                error: err,
            })
        
        console.log("SENT: ", meetings)

        return res.json({
            meeetings: meetings
        })
    })
}


// POST: Create new meeting
exports.new = function (req, res) {
    var meeting = new Meeting()
    meeting.name = req.body.name
    //meeting.models = req.body.models

    meeting.models =  [default_target, default_target, default_target]
    meeting.scales =  [default_scale, default_scale, default_scale]

    // save the meeting and check for errors
    meeting.save(function (err) {
    if (err)
        return res.json({
            message: "Something went wrong while creating new meeting",
            error: err
        })

    return res.json({
            message: 'New meeting created!',
            data: meeting
        })
    })
}


// GET: retrieve meeting info by meeting_id
exports.view = function (req, res) {
    Meeting.findById(req.params.meeting_id, function (err, meeting) {

        console.log("MEETING DETAILS: ", req.params.meeting_id)
        if (err || meeting == null)
            return res.json({
                message: 'Meeting not found',
                error: err
            })

        let models_ids = meeting.models
        let model_names = []
        Model.find({'uuid': { $in: models_ids }}, (model) => {
            if(model)
                model_names.push(model.name)
            else 
                model_names.push("no model here")
                
            console.log("SENT: ", meeting.models,"\n\n")

            return res.json({
                models: models_ids,
                names : model_names
            })
        })
        
    })
}

// UPDATE: update meeting info by meeting_id
exports.update = function (req, res) {
    Meeting.findById(req.params.meeting_id, function (err, ) {
        if (err || meeting == null)
            return res.json({
                message: "Meeting not found",
                error: err
            })

        meeting.name = req.body.name
        meeting.models = req.body.models
        
        // save the meeting and check for errors
        meeting.save(function (err) {
            if (err)
                return res.json({
                    message: "Something went wrong while updating meeting",
                    error: err
                })
            return res.json({
                message: 'Meeting Info updated',
                data: meeting
            })
        })
    })
}

// DELETE: delete meeting by meeting_id
exports.delete = function (req, res) {
    Meeting.deleteOne({_id: req.params.meeting_id}, function (err, meeting) {
        if (err)
            return res.json({
                message: "Something went wrong",
                error: err
            })

        return res.json({message: 'Meeting deleted'})
        })
}