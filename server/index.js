let express = require('express')
let bodyParser = require('body-parser')
let fileUpload = require('express-fileupload')
let api_routes = require('./api-routes')
let RoutesHanlder = require('./routes_handlers')
const Const = require('./constants')

const path = require('path')
const http = require('http')
const hbs = require('hbs')
const WebSocket = require('ws');

var ws_clients = {}
var ws_counter = 0

var streaming_clients = {}
var currently_streaming_device = ""

//create app and websocket
let app = express()
let port = process.env.PORT || 8100
let ws_port = process.env.WS_PORT || 8101

// configure app
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(fileUpload())

app.use('/public', express.static(path.join(__dirname, '..', 'public')))

app.set('view engine', 'hbs')
app.set('views', __dirname + '/../views');
hbs.registerPartials(path.join(__dirname, '..', 'views', 'partials'))
app.use('/api', api_routes) //Set api routes



//start websocket server
const httpServer = http.createServer(app)
const wss = new WebSocket.Server({ 'server': httpServer })
const requestHandler = new RoutesHanlder(wss)


app.get("/wstest", (req, res) => { return requestHandler.wsTest(req, res) })

app.get("/reset", (req, res) => { return requestHandler.resetMeeting(req, res) })

//meetings list page
app.get('/', (req, res) => { return requestHandler.index(req,res) })

//meeting details page
app.get('/details/', async (req, res) => { return requestHandler.details_page(req, res) })

//shared screenshots page
app.get('/screenshots', (req, res) => { return requestHandler.screenshots_page(req, res)} )


//upload model: create new model and update meetings models list
app.post('/upload', (req, res) => { return requestHandler.upload_model(req, res) })

//update model scale for target meeting
app.post('/scale', (req, res) => { return requestHandler.update_scale(req, res) })


//update model scale for target meeting
app.post('/light', (req, res) => { return requestHandler.update_light1(req, res) })


//send meeting details json
app.get('/api/meeting/:meeting_id', async (req, res) => { return requestHandler.meeting_details(req, res) })

//meeting screenshots urls
app.get('/api/meeting/screenshots/:meeting_id', async (req, res) => { return requestHandler.meeting_screenshots_list(req, res) })


//upload new screenshot
app.post('/screenshot', function(req, res){ return requestHandler.upload_screenshot(req, res) })

//save annotation
app.post('/view', function(req, res){ return requestHandler.save_view(req, res) })

//send point
app.post('/point', function(req, res){ return requestHandler.send_point_update(req, res) })

//toggle functionalities
app.post('/settings', function(req, res){ return requestHandler.send_new_settings(req, res) })

app.post('/lockedView', function(req, res){ return requestHandler.send_locked_view_update(req, res) })

app.post('/forceStop', function(req, res){ return requestHandler.send_force_stop(req, res) })


//page not found
app.use(function (req, res, next) { res.status(404).render('404.hbs') })
  

wss.on('connection', function connection(ws, req) {

	let client_ip = req.connection.remoteAddress;
	console.log("\nWSS - CONNECTION REQUEST FROM: ", client_ip)

	ws.on('message', data => {

		ws_counter++

		var logMsg = true
		try{

			let incomingWSUpdate = JSON.parse(data)
			console.log("\nWSS - NEW MESSAGE["+ ws_counter +"] FROM ", incomingWSUpdate.deviceName, "  ->", Const.UPDATE_NAMES[incomingWSUpdate.type])

			switch(incomingWSUpdate.type){

				//register new client
				case Const.UPDATE.REGISTER_DEVICE:

					ws_clients[incomingWSUpdate.deviceId] = ws
					ws_clients[incomingWSUpdate.deviceId].send(JSON.stringify({
						type: "ping",
						msg: "REGISTERED"
					}))

					streaming_clients[incomingWSUpdate.deviceId] = false

					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					
					break

				//delete registered device
				case Const.UPDATE.DELETE_DEVICE:

					var device_id = incomingWSUpdate.deviceId
					if(ws_clients[device_id])
						delete ws_clients[device_id]
					
					streaming_clients[incomingWSUpdate.deviceId] = false

					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					break
				
				//update light conditions
				case Const.UPDATE.LIGHT:

					requestHandler.update_light(incomingWSUpdate)

					break
	
				//update main image in gallery
				case Const.UPDATE.CHANGE_IMAGE:

					requestHandler.update_main_image(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					
					break

				//point on specific point
				case Const.UPDATE.POINT:
					
					requestHandler.send_point_update(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					
					break

				//stream view to other devices
				case Const.UPDATE.SHARE:
					
					requestHandler.send_share_update(incomingWSUpdate)

					if(! streaming_clients[incomingWSUpdate.deviceId]){
						
						for(var stremaing_client in streaming_clients)
							stremaing_client = false

						streaming_clients[incomingWSUpdate.deviceId] = true
						requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					}
					
					break

				//stream view to other devices
				case Const.UPDATE.STOP_SHARE:
					
					requestHandler.send_stop_share_update(incomingWSUpdate)

					streaming_clients[incomingWSUpdate.deviceId] = false
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)

					
					break

				//lock view for other devices
				case Const.UPDATE.LOCK:
					
					requestHandler.send_lock_update(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					
					break

				//unlock view for other devices
				case Const.UPDATE.STOP_LOCK:
					requestHandler.send_stop_lock_update(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					break

				//start quick real time annotation
				case Const.UPDATE.QUICK_ANNOTATION_START:
					
					requestHandler.send_quick_annotation_start(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)

					break

				//send quick annotation point
				case Const.UPDATE.QUICK_ANNOTATION:
					requestHandler.send_quick_annotation_point(incomingWSUpdate)
					break

				//stop quick real time annotation
				case Const.UPDATE.QUICK_ANNOTATION_STOP:
					
					requestHandler.send_quick_annotation_stop(incomingWSUpdate)
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)

					break

				//SPECIAL CASES LOGS
				case Const.UPDATE.LIGHT_LOG:
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					break

				case Const.UPDATE.LOAD_VIEW:
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					break

				case Const.UPDATE.SWITCH_MODE:
					requestHandler.log(incomingWSUpdate.deviceId,incomingWSUpdate.deviceName, Const.UPDATE_NAMES[incomingWSUpdate.type], data)
					break

				default:
					logMsg = false
					console.log("WSS - UNKNOW MSG TYPE - ",incomingWSUpdate)
					break
			}
		}
		catch(err){ console.log("WSS - ERROR ON MESSAGE\n", data, err) }
	})
})


// Launch app to listen to specified port
httpServer.listen(port, () => {
	console.log("Running XR-AR-Server on PORT: "+ port)
	console.log("WSS on PORT: "+ ws_port)
})