$(document).ready(function() {

    const UPDATE = {
        ADD_IMAGE:          5,
        CHANGE_IMAGE:       6,
        DELETE_IMAGE:       7,
        IMAGE_ANNOTATION:   8,
        REGISTER_DEVICE:    9,
        DELETE_DEVICE:     10,
    }

    var debug = Math.floor(Math.random() * 10)
    
    debug = 0
    
    const DEVICE_ID = 'BIGSCREEN' + debug
    const DEVICE_NAME = 'Big Screen # ' +debug

    var ws = null

    function startWebSocket(){

        var current_image = 0

        try{
            ws = new WebSocket("wss://xdar-au.herokuapp.com")
            // ws = new WebSocket("ws://localhost:8100")
        }
        catch(error){ console.log("WS - ERROR: failed to open connection with WS server")}
        
        ws.onerror = function(err){ console.log("WS - ERROR", err) }

        ws.onopen = function(){ 
            
            let msg = JSON.stringify({
                type : UPDATE.REGISTER_DEVICE,
                deviceId: DEVICE_ID,
                deviceName: DEVICE_NAME
            })

            console.log("WS - REGISTERING")
            ws.send( msg)
        }

        ws.onmessage = function(e){

            if(e){

                let msg = JSON.parse(e.data)
                console.log("WS - MESSAGE FROM SERVER\n", msg)
        
                //add new screenshot
                if(msg.type == UPDATE.ADD_IMAGE || msg.update == "new_image"){
                    
                    console.log("WS - IMAGE UPDATE REQUEST")
                    let new_img_url = msg.url
                    
                    console.log(new_img_url)
                    
                    addNewImage(new_img_url)
                    changeImage(new_img_url)
                }

                //change displayed image
                if(msg.update == UPDATE.CHANGE_IMAGE || msg.type == UPDATE.CHANGE_IMAGE){
                    
                    console.log("CHANGING IMAGE")
                    var new_image_index = msg.uuid
                    if(new_image_index != current_image){

                        current_image = new_image_index

                        url = $($('.thumbnail-img')[new_image_index]).prop('src')
                        changeImage(url)
                   }

                   var new_image_index = msg.index
                    if(new_image_index != current_image){

                        current_image = new_image_index

                        url = $($('.thumbnail-img')[new_image_index]).prop('src')
                        changeImage(url)
                   }
                }
            }
        }

        ws.onclose = function(){

            console.log('WS - CONNECTION LOST')
            checkConectionState()
        }
    }

    function checkConectionState(){
        if(!ws || ws.readyState === ws.CLOSED) 
            startWebSocket()
    }

    //Open WS connection with server
    startWebSocket()
    setInterval(checkConectionState, 1000)

    //Initialize canvas: get canvas 2D context and set its size
    let canvas = document.getElementById('main-canvas')
    let ctx = canvas.getContext('2d')
    let pos = { x: 0, y: 0 }
    initCanvas()


    //change main image
    $('.sidebar').on('click', '.thumbnail-img', (e)=> {
        
        let imr_src_url = $(e.currentTarget).prop("src")
        current_image = $(e.currentTarget).data("id")

        changeImage(imr_src_url, true)
    })


    /* UTILS */

    function changeImage(img_url, notify=false){

        $('.main-img').prop("src", img_url)
        resizeCanvas()

        

        if(notify){
            
            var updateImageMsg = JSON.stringify({
                type : UPDATE.CHANGE_IMAGE,
                index: current_image,
                deviceId: DEVICE_ID,
                deviceName : DEVICE_NAME
            })

            ws.send(updateImageMsg)
        }
    }

    function addNewImage(img_url){

        let data_id = $('.thumbnail-container').length + 1
        let new_img = $('<div class="thumbnail-container"><img class="thumbnail-img" src="'+ img_url + '" data-id="' + data_id + '"></div>')

        $('.sidebar').append(new_img)
    }


    /* CANVAS UTILS */

    function resizeCanvas() {

        console.log("Resizing canvas")
        ctx.canvas.width = canvas.offsetWidth
        ctx.canvas.height = canvas.offsetHeight
    }

    function drawOnCanvas(e) {
        // mouse left button must be pressed
        if (e.buttons !== 1) return

        ctx.beginPath() // begin

        ctx.lineWidth = 5
        ctx.lineCap = 'round'
        ctx.strokeStyle = '#ffbbcc'

        ctx.moveTo(pos.x, pos.y) // from
        setPosition(e)
        ctx.lineTo(pos.x, pos.y) // to

        ctx.stroke() // draw it!
    }

    // new position from mouse event
    function setPosition(e) {

        pos.x = e.clientX - canvas.getBoundingClientRect().x
        pos.y = e.clientY - canvas.getBoundingClientRect().y
    } 

    function initCanvas() {

        resizeCanvas()

        window.addEventListener('resize', resizeCanvas)
        document.addEventListener('mousemove', drawOnCanvas)
        document.addEventListener('mousedown', setPosition)
        document.addEventListener('mouseenter', setPosition)
    }
})
