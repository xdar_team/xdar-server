//Dropzone.autoDiscover = false;
$(document).ready(function() {
    $(".btn-scale").on('click', (e) => {

        let target_id = $(e.currentTarget).attr('data-target-id')
        let scale = $('.scale-' + target_id).val()

        let data = {
            scale: scale,
            target_id : target_id
        }

        console.log("Sending scale update request..")
        $.ajax({
            type: "post",
            url: "/scale",
            data: data,
            success: function (response) {
                console.log("Model scaled")
                console.log(response.data)
                if(response.status == 200) {
                    console.log("scale sent")
                } else if(response.status === "error") {
                    console.log("ops")
                }
            }
        })
    })

    $(".btn-delete").on('click', (e) => {

        let target_id = $(e.currentTarget).attr('data-target-id')
        let data = {target_id : target_id}

        $.ajax({
            type: "post",
            url: "/delete",
            data: data,
            success: function (response) {
                console.log("Model DELETED")
                if(response.status == 200) {
                    console.log("delete sent")
                } else if(response.status === "error") {
                    console.log("ops")
                }
            }
        })
    })
})

$(document).on('input change', '#slider', function() {
    let light_val = $(this).val() 
    
    console.log("SENDING REQUEST CHANGE LIGHT TO: "+ light_val)

    let data = { light_val: light_val }

    $.ajax({
        type: "post",
        url: "/light",
        data: data,
        success: function (response) {
            console.log("request sent success")
            if(response.status == 200) {
                console.log("scale sent")
            } else if(response.status === "error") {
                console.log("ops")
            }
        }
    })
});
function refreshPage(){
    console.log($("upload completed"))
    location.reload()
}